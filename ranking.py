SPACE = " "
GREATER = ">"

def parse_line(line: str) -> list:
    name, *marks = line.strip().split()
    marks = [int(_) for _ in marks]
    return [name] + marks

def load_data(mark_file: str) -> list:
    return [parse_line(line) for line in open(mark_file)]

print(load_data("studentRanking.txt"))

def rank(mark_list: list[list[str]]) -> list[str]:
    rank_list = []
    for i in range(len(mark_list) - 1):
        for k in range(i + 1, len(mark_list)):
            count = 0
            for j in range(1, len(mark_list[i])):
                if int(mark_list[i][j]) > int(mark_list[k][j]):
                    count += 1
            if count == len(mark_list[i]) - 1:
                rank_list.append(mark_list[i][0] + GREATER + mark_list[k][0])
    return rank_list

print (rank (load_data("studentRanking.txt")))